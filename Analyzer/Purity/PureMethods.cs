﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using Analyzer.Attributes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;

namespace Analyzer.Purity
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class PureMethods
    {
        private static readonly IList<Type> SuppressionAttributes = new List<Type>
        {
        };

        public static readonly AnalyzerDetails Details =
            new AnalyzerDetails("StaticMethodsShouldNotBeVoid",
                                AnalyzerCategories.PureFunctionAnalyzers,
                                DefaultState.EnabledByDefault,
                                DiagnosticSeverity.Warning,
                                "PureMethod", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidTitle),
                                "", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidDescription),
                                "", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidMessageFormat),
                                SuppressionAttributes);

        public static readonly DiagnosticDescriptor Rule = Details.GetDiagnosticDescriptor();

        public void AnalyzeSymbol(CodeBlockAnalysisContext context)
        {
            var block = context.CodeBlock;
            if (block.Language != "C#") 
                return;

            if(block is MethodDeclarationSyntax method)
            {
                if (!method.AttributeLists.SelectMany(x => x.Attributes).ToList().Any(x => x.Name.ToString() == "Pure")) 
                    return;

                foreach(var statement in method.Body.Statements)
                {
                    // simple member assignment;
                    if(statement is ExpressionStatementSyntax exprStatement && exprStatement.Expression is AssignmentExpressionSyntax assignExpr)
                    {
                        if(assignExpr.Left.)
                        {

                        }
                    }
                }

            }
            // if(block)
            // suppress symbols

            // if (block.IsStatic &&
            //     block.ReturnsVoid &&
            //     block.MethodKind != MethodKind.PropertySet &&
            //     block.Name.ToString() != "Main"
            //     // &&
            //     //!CommonFunctions.SkipSymbolAnalysis(methodSymbol, _settingsHandler, SuppressionAttributes)
            //     )
            // {
            //     context.ReportDiagnostic(Diagnostic.Create(Rule, block.Locations[0], block.Name));
            // }
        }
    }
}
