﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using Analyzer.Attributes;

namespace Analyzer.Purity
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class StaticMethodsShouldNotBeVoid
    {
        private static readonly IList<Type> SuppressionAttributes = new List<Type>
        {
            typeof(HasSideEffectsAttribute),
            typeof(MutatesParameterAttribute)
        };

        public static readonly AnalyzerDetails AA1000Details =
            new AnalyzerDetails("StaticMethodsShouldNotBeVoid",
                                AnalyzerCategories.PureFunctionAnalyzers,
                                DefaultState.EnabledByDefault,
                                DiagnosticSeverity.Warning,
                                "PurityRoslynAnalyzer", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidTitle),
                                "", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidDescription),
                                "?", //nameof(Resources.AA1000StaticMethodsShouldNotBeVoidMessageFormat),
                                SuppressionAttributes);

        public static readonly DiagnosticDescriptor Rule = AA1000Details.GetDiagnosticDescriptor();

        public void AnalyzeSymbol(SymbolAnalysisContext context)
        {
            var methodSymbol = (IMethodSymbol)context.Symbol;
            // suppress symbols

            if (methodSymbol.IsStatic &&
                methodSymbol.ReturnsVoid &&
                methodSymbol.MethodKind != MethodKind.PropertySet &&
                methodSymbol.Name.ToString() != "Main"
                // &&
                //!CommonFunctions.SkipSymbolAnalysis(methodSymbol, _settingsHandler, SuppressionAttributes)
                )
            {
                context.ReportDiagnostic(Diagnostic.Create(Rule, methodSymbol.Locations[0], methodSymbol.Name));
            }
        }
    }
}
