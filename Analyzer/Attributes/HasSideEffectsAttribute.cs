﻿using System;

namespace Analyzer.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class HasSideEffectsAttribute : Attribute, IAttributeDescriber
    {
        public string AttributeDescription =>
            $"A static method annotated with {nameof(HasSideEffectsAttribute)} is allowed to be void and have no " +
            "parameters as it is explicitly declaring its intent to cause side effects.";
    }
}
