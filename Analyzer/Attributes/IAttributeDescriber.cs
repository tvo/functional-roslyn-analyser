﻿namespace Analyzer.Attributes
{
    public interface IAttributeDescriber
    {
        string AttributeDescription { get; }
    }
}
