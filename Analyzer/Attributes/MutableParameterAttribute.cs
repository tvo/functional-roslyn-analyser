﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analyzer.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public sealed class MutableParameterAttribute : Attribute, IAttributeDescriber
    {
        public MutableParameterAttribute(string firstMutableParameter, params string[] subsequentMutableParameters) { }
        private MutableParameterAttribute() { }

        public string AttributeDescription =>
            $"A method annotated with {nameof(MutableParameterAttribute)} is allowed to have parameters that change value (ie can" +
            $"be reassigned. The parameters affected must be named in the {nameof(MutableParameterAttribute)} attribute, eg" +
            $"{Environment.NewLine}````cs{Environment.NewLine}" +
            $"[{nameof(MutableParameterAttribute)}(\"p1\")]{Environment.NewLine}" +
            $"public void F(int p1, int p2)Environment.{Environment.NewLine}{{{Environment.NewLine}" +
            $"    p1 = 1;{Environment.NewLine}p2 = 2;{Environment.NewLine}}}{Environment.NewLine}````{Environment.NewLine}" +
            $"would result in no errors over the reassignment of `p1`.{Environment.NewLine}" +
            $"Multiple variables can be supplied to the attribute, eg [{nameof(MutableParameterAttribute)}(\"p1\", \"p2\")]";
    }
}
