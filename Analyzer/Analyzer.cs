using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Analyzer.Purity;

namespace Analyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class Analyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => new DiagnosticDescriptor[]{
            StaticMethodsShouldNotBeVoid.Rule,
            PureMethods.Rule,
        }.ToImmutableArray();

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSymbolAction(new StaticMethodsShouldNotBeVoid().AnalyzeSymbol, SymbolKind.Method);
            context.RegisterCodeBlockAction(new PureMethods().AnalyzeSymbol);
            
            // TODO: Consider registering other actions that act on syntax instead of or in addition to symbols
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Analyzer%20Actions%20Semantics.md for more information
        }
    }
}
