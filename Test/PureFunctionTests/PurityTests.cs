using Microsoft.CodeAnalysis;
using TestHelper;
using Xunit;
using FluentAssertions;
using Analyzer.Purity;
using System.Linq;

namespace PurityRoslynAnalyzer.Test
{
    public class PurityTests : CodeFixVerifier
    {
        [Fact]
        public void PureStaticInt_MutatesSate_Fails()
        {
            var test =
@"
using System.Diagnostics.Contracts;
class FailClass
{
    static int mutatesMe = 1;
    [Pure]
    static int ThisMethodShouldFail()
    {
        mutatesMe = 2;
        return mutatesMe;
    }
}
";
            var diagnostics = GetCSharpDiagnostic(test);

            // StaticMethodsShouldNotBeVoid.AA1000Details
            diagnostics.Should().NotBeNullOrEmpty();
            diagnostics.Single().Severity.Should().Be(DiagnosticSeverity.Warning);
        }
        [Fact]
        public void PureStaticMethodReturnsInt_Passes()
        {
            var test =
@"
class TestClass
{
    [Pure]
    static int ReturnsOne() => 1;
}
";
            var diagnostics = GetCSharpDiagnostic(test);

            diagnostics.Should().BeNullOrEmpty();
        }
    }
}
