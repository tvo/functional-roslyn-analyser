using Microsoft.CodeAnalysis;
using TestHelper;
using Xunit;
using FluentAssertions;
using Analyzer.Purity;
using System.Linq;

namespace PurityRoslynAnalyzer.Test
{
    public class StaticMethodsShouldNotBeVoidTests : CodeFixVerifier
    {
        [Fact]
        public void StaticVoid_Fails()
        {
            var test =
@"
class FailClass
{
    static void ThisMethodShouldFail()
    {
    }
}
";
            var diagnostics = GetCSharpDiagnostic(test);

            // StaticMethodsShouldNotBeVoid.AA1000Details
            diagnostics.Should().NotBeNullOrEmpty();
            diagnostics.Single().Severity.Should().Be(DiagnosticSeverity.Warning);
        }
        [Fact]
        public void StaticMethodReturnsInt_Passes()
        {
            var test =
@"
class TestClass
{
    static int ReturnsOne() => 1;
}
";
            var diagnostics = GetCSharpDiagnostic(test);

            diagnostics.Should().BeNullOrEmpty();
        }
    }
}
