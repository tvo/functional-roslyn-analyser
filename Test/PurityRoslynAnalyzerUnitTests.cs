using Microsoft.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHelper;

namespace PurityRoslynAnalyzer.Test
{
    [TestClass]
    public class UnitTest : CodeFixVerifier
    {
        [TestMethod]
        public void Simple_Console_HelloWorld_NoErrors()
        {
            var test =
@"using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(""Hello World!"");
        }
    }
}
";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void Impure_Test_Function_Fails()
        {
            var test =
@"using System;
using System.Diagnostics.Contracts;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var prgm = new Program();

            prgm.Test();

            Console.WriteLine(""Hello World!"");
        }

        string mutableProperty = """";
        [Pure]
        public void Test()
        {
            mutableProperty = ""asdf"";
        }
    }
}
";
            
            VerifyCSharpDiagnostic(test);
        }
    }
}
